# blockbench-hinge-rotation

A plugin for [Blockbench](https://github.com/JannisX11/blockbench) for rotating cubes around a hinge.
