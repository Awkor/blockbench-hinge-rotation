(function () {
  var hinge_base;
  var hinge_direction;
  var hinge_rotation_tool;
  var hinge_top;
  var original_origin;
  var rotating_cube;
  var selected_cube;
  var selected_vertices = [];
  var vertex_cubes = [];
  Plugin.register("hinge_rotation", {
    title: "Hinge Rotation",
    author: "Awkor",
    icon: "settings_backup_restore",
    description: "Tool for rotating cubes around a hinge",
    version: "0.1.0",
    variant: "both",
    onload() {
      hinge_rotation_tool = new Tool({
        id: "hinge_rotation",
        name: "Hinge Rotation",
        description: "Tool for rotating cubes around a hinge",
        icon: "settings_backup_restore",
        transformerMode: "hidden",
        category: "tools",
        alt_tool: "move_tool",
        modes: ["edit"],
        onCanvasClick: function (data) {
          HingeRotation.canvasClick(data);
        },
        onUnselect: function () {
          HingeRotation.unselect();
        },
      });
      Toolbox.add(hinge_rotation_tool);
    },
    onunload() {
      hinge_rotation_tool.delete();
    },
  });
  const HingeRotation = {
    canvasClick(data) {
      cube = data.cube;
      if (!cube) {
        return;
      }
      let vertex_cube_index = vertex_cubes.indexOf(cube);
      if (vertex_cube_index > -1) {
        // The user selected a vertex cube.
        vertex_cube = vertex_cubes[vertex_cube_index];
        // Set the selection back to the main cube since the vertex cubes should be ephemeral.
        selected_cube.select();
        let vertex_cube_center = vertex_cube.getWorldCenter();
        selected_vertices.push(vertex_cube_center);
        if (selected_vertices.length === 2) {
          // Save a reference of the currently selected cube.
          rotating_cube = selected_cube;
          // Set the hinge onto which rotate around.
          hinge_base = selected_vertices[0];
          hinge_top = selected_vertices[1];
          hinge_base = rotating_cube.mesh.worldToLocal(hinge_base);
          hinge_top = rotating_cube.mesh.worldToLocal(hinge_top);
          hinge_direction = hinge_top.clone().sub(hinge_base).normalize();
          // Change the cube's pivot point.
          original_origin = [...rotating_cube.origin];
          let origin = hinge_top.clone();
          origin = rotating_cube.mesh.localToWorld(origin).toArray();
          rotating_cube.transferOrigin(origin);
        } else if (selected_vertices.length === 4) {
          // Rotate the cube.
          let rotating_vertex = selected_vertices[2];
          let target_vertex = selected_vertices[3];
          this.rotateCube(rotating_vertex, target_vertex);
          // Restore the cube's original pivot point.
          rotating_cube.transferOrigin(original_origin);
          this.unselect();
        }
      } else {
        if (selected_cube) {
          // The user had already selected a cube with this tool.
          if (cube === selected_cube) {
            // The user clicked on the already selected cube, do nothing.
            return;
          } else {
            // The user clicked on a different cube, remove the vertex cubes of the previously selected cube.
            this.removeVertexCubes();
          }
        }
        this.selectCube(cube);
      }
    },
    addVertexCubes(cube) {
      let vertices = cube.mesh.geometry.vertices;
      let size = new THREE.Vector3(0.25, 0.25, 0.25);
      vertices.forEach((vertex, index) => {
        let vertex_cube = new Cube({
          name: cube.name + "-vertex-" + index,
          origin: cube.origin,
          rotation: cube.rotation,
          size: size.toArray(),
        });
        let group = getCurrentGroup();
        vertex_cube.addTo(group).init();
        {
          // Move the vertex cube to the vertex's position.
          let origin = new THREE.Vector3();
          origin = origin.fromArray(cube.origin);
          let position = vertex.clone().add(origin);
          position = position.toArray();
          vertex_cube.moveVector(position);
        }
        {
          // Offset the vertex cube so its center coincides with the vertex's position.
          let offset = size.clone().multiplyScalar(-0.5);
          offset = offset.toArray();
          vertex_cube.moveVector(offset);
        }
        vertex_cubes.push(vertex_cube);
      });
    },
    removeVertexCubes() {
      vertex_cubes.forEach((cube) => {
        cube.remove();
      });
      vertex_cubes = [];
    },
    rotateCube(rotating_vertex, target_vertex) {
      rotating_vertex = rotating_cube.mesh.worldToLocal(rotating_vertex);
      target_vertex = rotating_cube.mesh.worldToLocal(target_vertex);
      // Use the hinge's direction as a normal vector of a plane.
      let plane_normal = hinge_direction.clone();
      // By projecting the target vertex's vector onto the plane, we'll get the angle delta on the rotation plane of the hinge.
      target_vertex.projectOnPlane(plane_normal);
      // Get the angle between the two vectors.
      let angle = rotating_vertex.angleTo(target_vertex);
      angle = Math.radToDeg(angle);
      // Determine the angle's sign: https://stackoverflow.com/questions/5188561
      let cross = rotating_vertex.cross(target_vertex);
      let dot = plane_normal.dot(cross);
      angle = angle * Math.sign(dot);
      // Rotate the cube.
      rotation = hinge_direction.clone().multiplyScalar(angle);
      rotating_cube.rotation.V3_add(rotation);
      Canvas.adaptObjectPosition(rotating_cube);
      Canvas.adaptObjectFaces(rotating_cube);
      Canvas.updateUV(rotating_cube);
    },
    unselect() {
      hinge_base = null;
      hinge_direction = null;
      hinge_top = null;
      original_origin = null;
      rotating_cube = null;
      selected_cube = null;
      selected_vertices = [];
      if (vertex_cubes.length > 0) {
        this.removeVertexCubes();
      }
    },
    selectCube(cube) {
      selected_cube = cube;
      this.addVertexCubes(cube);
    },
  };
})();
